from django.db import models
from django.forms.widgets import *
from django.core.mail import send_mail, BadHeaderError
from django import forms

# Create your models here.
# A simple contact form with four fields.
class ContactForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    topic = forms.CharField()
    message = forms.CharField(widget=Textarea())
